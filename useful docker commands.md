After installation, start docker service (on AMI-linux):
`sudo service docker start`

Setting docker to start on boot (on AMI-linux):
`sudo chkconfig docker on`

After installation, allow user simon to run docker:
`usermod -a -G docker simon`

Check with:
`cat /etc/group | grep docker`
which should now display:
`docker:x:987:simon`


To display list of images:
`docker images`

To list all currently running containers:
`docker ps`

To list all recently running containers:
`docker ps -a`

-it with /bin/bash will start an interactive prompt
`docker run -it centos /bin/bash`

-d will start the image in background:
`docker run -d nginx` 

--name will give the container a name:
`docker run -d --name=MyName nginx`

To attach to a process in a container:
`docker attach MyName`

If a container is done running, you can start it with 
`docker start MyName`

If a container is still running, you can restart it with 
`docker restart MyName`

To start a bash prompt on a running container:
`docker exec -it MyName /bin/bash`

To remove an image:
`docker rmi centos`
However, be aware that you cannot remove an image if any container has been 
instantiated from it.

You can force remove it anyway with:
`docker rmi -f centos`

But you should rather remove dependent containers anyway:
`docker rm MyName`

Delete all non-running containers:
``docker rm `docker ps -a -q` ``

Links:
https://docs.docker.com/install/linux/linux-postinstall/
