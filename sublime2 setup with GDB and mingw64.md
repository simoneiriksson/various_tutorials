<h1>This todo helps me install MingW and Sublime and set up the build and debugger correctly
</h1>
<h2> Install stuff </h2>
* Install Mingw from MingW-W64-builds: 
    http://mingw-w64.org/doku.php/download/mingw-builds
    
    Add the bin-path (`C:\\mingw-w64\x86_64-8.1.0-posix-seh-rt_v6-rev0\mingw64\bin`)
    to the PATH environment variable

* Install Sublime text editor 2. Be aware that sublimeGDB does not support ST3.
    https://www.sublimetext.com/2

* Install Package Control in Sublime2 by following this link: 
    https://packagecontrol.io/installation#st2

* Restart Sublime2 three times!

* Follow the rest of the instructions here: https://packagecontrol.io/packages/SublimeGDB
    * Once package control has been installed, bring up the command palette (cmd+shift+P or ctrl+shift+P)

    * Type Install and select "Package Control: Install Package"

    * Select SublimeGDB from the list. Package Control will keep it automatically updated for you

<h2> Setup Sublime to build C++ with debugging symbols</h2>

* In Sublime2, create a file named Mingw C++ build.sublime-build in the folder:
    `C:\Users\[USER]\AppData\Roaming\Sublime Text 2\Packages\User`
And add the following to the file:
```
{
    "file_regex": "^(..[^:]*):([0-9]+):?([0-9]+)?:? (.*)$",
    "working_dir": "${file_path}",
    "selector": "source.c, source.cpp, source.c++",
    "shell": true,
    "path": "C:\\mingw-w64\\x86_64-8.1.0-posix-seh-rt_v6-rev0\\mingw64\\bin",
    "cmd": ["g++.exe", "-o", "${file_base_name}.exe", "-static-libgcc", "-static-libstdc++", "*.cpp"],
    "variants": [
        {
            "name": "Run",
            "cmd": ["g++.exe", "-o", "${file_base_name}.exe", "-static-libgcc", "-static-libstdc++", "*.cpp", "&", "${file_base_name}.exe"]
        },
        {
            "name": "Debug",
            "cmd": ["g++.exe", "-o", "${file_base_name}.exe", "-static-libgcc", "-static-libstdc++", "-g", "*.cpp"]       
        }

    ]
}
```

* In Sublime2, select the menu Preference->User, which will open the file 
    `C:\Users\[USER]\AppData\Roaming\Sublime Text 2\Packages\User\Default (Windows).sublime-keymap`
Add the following line to the file: 
```
{ "keys": ["ctrl+alt+b"], "command": "build", "args": {"variant": "Debug"} }
```
You may find the general keybinding here: Preference->User.

<h2> Setup Sublime to work with gdb</h2>

* To set up SublimeGDB with Mingw:
Copy the file 
    `C:\Users\[USER]\AppData\Roaming\Sublime Text 2\Packages\SublimeGDB\SublimeGDB.sublime-settings`
to 
    `C:\Users\[USER]\AppData\Roaming\Sublime Text 2\Packages\User\SublimeGDB.sublime-settings`

and change the following settings:
```
"workingdir": "${folder:${file}}",
"commandline": "C:\\mingw-w64\\x86_64-8.1.0-posix-seh-rt_v6-rev0\\mingw64\\bin\\gdb --interpreter=mi ${file_base_name}.exe",
"server_workingdir": "C:\\mingw-w64\\x86_64-8.1.0-posix-seh-rt_v6-rev0\\mingw64\\bin\\",
"server_commandline": "C:\\mingw-w64\\x86_64-8.1.0-posix-seh-rt_v6-rev0\\mingw64\\bin\\gdbserver.exe",
```